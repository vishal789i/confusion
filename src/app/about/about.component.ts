import { Component, OnInit, Inject } from '@angular/core';
import { Leader } from '../shared/Leader';
import { LeaderService } from '../services/leader.service';
import { LEADERS } from '../shared/leaders';
import { flyInOut, expand } from '../annimations/app.annimations';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class AboutComponent implements OnInit {

  leaders: Leader[];

  constructor(private leaderService:LeaderService, @Inject('BaseURL') private BaseURL ) { }

  ngOnInit() {
    this.leaderService.getLeaders().subscribe( leaders => this.leaders = leaders);
                                        //this is argument to function, second leaders is the class member leaders
                                        //third leader is the function argument leaders that is being assigned to the class member leaders
  }
 
}
