import { Component, OnInit, ViewChild, Injectable, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DishService } from '../services/dish.service';

import { Comment } from '../shared/comment';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { switchMap } from 'rxjs/operators'

import { visibility } from '../annimations/app.annimations'

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  animations: [
    visibility()
  ]
})
export class DishdetailComponent implements OnInit {

  dish: Dish;
  comment: Comment;
  commentForm: FormGroup;
  date: string;
  @ViewChild('cform') commentFormDirective;

  dishIds: string[];
  prev: string;
  next: string;
  errMess: string;
  dishcopy: Dish;

  visibility = 'shown';

  formErrors = {
    'author': '',
    'rating': '',
    'comment': '',
  };

  validationMessages = {
    'author': {
      'required':      'Author Name is required.',
      'minlength':     'Author Name must be at least 2 characters long.',
      'maxlength':     'Author Name cannot be more than 25 characters long.'
    },
    'rating': {
      'required':      'Last Name is required.'
    },
    'comment': {
      'required':      'A Comment is required.'
    },
  };
  
  constructor(private dishService: DishService, private route: ActivatedRoute, private location: Location, private fb: FormBuilder,
              @Inject('BaseURL') private BaseURL) { 
    this.createForm();
  }

  createForm() {
    this.commentForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(200)] ],
      rating: 5,
      comment: ['', [Validators.required] ]
    });

    this.commentForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); //reset validation messages now
  }


  onValueChanged(data?: any) {
    if (!this.commentForm) { return; }
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = this.commentForm.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }


  ngOnInit() {
    this.dishService.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.params
      .pipe(  switchMap(  (params: Params) => { this.visibility = 'hidden'; return this.dishService.getDish(params['id']);  }  )  )
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
        errmess => this.errMess = <any>errmess);  

      this.date = new Date().toISOString();

  }

  goBack(): void {
    this.location.back();
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  onSubmit() {
    this.comment = this.commentForm.value;
    this.comment['date'] = this.date;

    console.log(this.comment);
    
    this.dishcopy.comments.push(this.comment);

    this.dishService.putDish(this.dishcopy)
      .subscribe(dish => {
        this.dish = dish; this.dishcopy = dish;
      }, errmess => { this.dish = null; this.dishcopy = null; this.errMess = <any>errmess; });

    this.commentForm.reset({
      author: '',
      value: 5,
      comment: ''
    });
    this.commentFormDirective.resetForm();
  }



}